public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double area() {
        return Math.PI * radius * radius;

    }

    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    public boolean intersect(Circle circle) {
        int distanceX = circle.center.xCoord - center.xCoord;
        int distanceY = circle.center.yCoord - center.yCoord;
        double distanceCenters = Math.sqrt((distanceX * distanceX) + (distanceY * distanceY));
        int sumOfRadiuses = circle.radius + radius;
        if (distanceCenters > sumOfRadiuses) {
            return false;
        } else {
            return true;
        }
    }
}
