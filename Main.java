public class Main {
    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(4, 3, new Point(0, 0));
        System.out.println("Rectangle area: " + rectangle.area());
        System.out.println("Rectangle perimeter: " + rectangle.perimeter());
        Point[] points = rectangle.corners();
        for (int i = 0; i < points.length; i++) {
            System.out
                    .println("Rectangle's corner" + (i + 1) + ": (" + points[i].xCoord + "," + points[i].yCoord + ")");
        }
        Circle circle1 = new Circle(10, new Point(0, 0));
        System.out.println("Circle area: " + circle1.area());
        System.out.println("Circle perimeter: " + circle1.perimeter());
        Circle circle2 = new Circle(5, new Point(3, 4));
        System.out.println("Are they intersect(circle1 and circle2): " + circle1.intersect(circle2));

    }
}
